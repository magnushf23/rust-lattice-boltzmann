use std::path::Path;

use crate::{
    dimension::LbmDimension,
    gpu::Gpu,
    imagetexture::ImageTexture,
    parameters::RawSimulationParameters,
    problem_definition::ProblemDefinition,
    renderer::{RenderParameters, Renderer},
    simulationbuilder::SimulationBuilder,
    solver::GpuSolver,
};
use time::Instant;
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

pub struct Program {
    pub gpu: Gpu,
    pub solver: GpuSolver,
    pub renderer: Renderer,
    pub texture: ImageTexture,
    pub dimension: LbmDimension,
    pub it_per_render: usize,
}
impl Program {
    pub async fn new(window: &Window) -> Self {
        let init_time = Instant::now();

        let problem_file = Path::new("./examples/fluid_problems/injection.json");
        let problem = ProblemDefinition::from_json(problem_file).unwrap_or_else(|_| {
            panic!(
                "Problem while loading the file '{:}'",
                problem_file.display()
            )
        });
        let builder = SimulationBuilder::from_problem_definition(&problem);

        let params = problem.sim_parameters;
        dbg!(params);
        let raw_sim_parameters = RawSimulationParameters::from(&params);
        let resolution = params.dimension.resolution_u32();

        // Initialize the gpu controller
        let gpu = Gpu::new(window).await;

        let render_params = RenderParameters {
            size: [resolution[0], resolution[1]],
        };

        let texture = ImageTexture::new(
            &gpu,
            [resolution[0], resolution[1]],
            wgpu::TextureFormat::Rgba8Unorm,
        );

        let renderer = Renderer::new(&gpu, &render_params, &texture);
        let solver = GpuSolver::new(&gpu, &builder, &raw_sim_parameters, &texture);
        println!(
            "\nInitialisation done in {} seconds.\n",
            init_time.elapsed()
        );

        //gpu.surface.configure(&gpu.device, &gpu.config);

        Program {
            gpu,
            solver,
            renderer,
            texture,
            dimension: params.dimension,
            it_per_render: 1,
        }
    }

    pub fn input(&mut self, _event: &WindowEvent) -> bool {
        false
    }

    pub fn update(&mut self) {
        // Nothing
    }

    pub fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        // Create render output surface for sim visualization

        let output = self.gpu.surface.get_current_texture()?;
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = self
            .gpu
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Compute & Render Encoder"),
            });

        // Update simulation
        for _iteration in 0..self.it_per_render {
            self.solver.next_step(
                &self.gpu.queue,
                &mut encoder,
                &self.texture.compute_bind_group,
            );
        }

        // Render
        self.renderer
            .render(&mut encoder, &view, &self.texture.render_bind_group);

        self.gpu.queue.submit(std::iter::once(encoder.finish()));
        //self.gpu.queue.submit(Some(encoder.finish()));
        output.present();
        //println!("Renders...");
        Ok(())
    }
}

// Main run loop
pub async fn run() {
    {
        env_logger::init();
    };
    // Initialize event loop and window
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("Lattice Boltzmann Simulation")
        .build(&event_loop)
        .unwrap();

    // Build program
    let mut program = Program::new(&window).await;
    let mut stopwatch = Instant::now();
    let mut render_counter = 0;

    // Run
    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent {
            ref event,
            window_id,
        } if window_id == window.id() => {
            if !program.input(event) {
                match event {
                    WindowEvent::CloseRequested
                    | WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state: ElementState::Pressed,
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    } => *control_flow = ControlFlow::Exit,
                    WindowEvent::Resized(physical_size) => {
                        program.gpu.resize(*physical_size);
                    }
                    WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                        // new_inner_size is &&mut so we have to dereference it twice
                        program.gpu.resize(**new_inner_size);
                    }
                    _ => {}
                }
            }
        }
        Event::RedrawRequested(window_id) if window_id == window.id() => {
            match program.render() {
                Ok(_) => {
                    render_counter += 1;
                }
                // Reconfigure the surface if lost
                Err(wgpu::SurfaceError::Lost | wgpu::SurfaceError::Outdated) => {
                    program.gpu.resize(program.gpu.size)
                }
                // The system is out of memory, we should probably quit
                Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                // All other errors (Outdated, Timeout) should be resolved by the next frame
                Err(e) => eprintln!("{:?}", e),
            }
        }
        Event::MainEventsCleared => {
            // RedrawRequested will only trigger once, unless we manually
            // request it.
            let elapsed_time = stopwatch.elapsed().as_seconds_f64();
            if elapsed_time >= 1. {
                println!(
                    "It/s: {:.1} | Average invocation runtime: {:.2e} s/node | Sim time t = {:.2e} s",
                    ((render_counter * program.it_per_render) as f64 / elapsed_time),
                    elapsed_time / program.dimension.node_number() as f64 / render_counter as f64,
                    program.solver.current_simulation_time()
                );
                render_counter = 0;
                stopwatch = Instant::now();
            }
            window.request_redraw();
        }
        _ => {}
    });
}
