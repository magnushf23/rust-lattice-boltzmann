//! # Lattice Boltzmann fluid simulations on GPU !
//!
//! Simulations are executed by the `core::run()` async function which first creates a simulation context, and
//! then runs a loop that performs compute passes and render passes on the GPU using the wgpu crate.
//!

/// This defines the `Program` struct and the main run loop.
pub mod core;
/// Special struct for defining the dimensions of a LBM problem in the 1D, 2D and 3D case.
pub mod dimension;
/// Geometry tools for tracing objects in the simulation domain
pub mod geometry;
/// This module is for handling the GPU devices, surfaces, etc.
pub mod gpu;
/// Custom `ImageTexture` struct for the visualization of the simulation.
pub mod imagetexture;
/// Module for easy configuration and export of the physical parameters for the simulation.
pub mod parameters;
/// A JSON reader/writer for saving and load simulation problem definition.
pub mod problem_definition;
/// Raw data structures used for the GPU buffers
pub mod raw;
/// GPU pipeline for the rendering. Controls the vertex and fragment shaders.
pub mod renderer;
/// Structs for defining physical objects in the simulation scene.
pub mod scene_object;
/// Module for the creation and initialization of the Lattice Boltzmann fluid problem.
pub mod simulationbuilder;
/// GPU fluid solver. Controls the compute shader.
pub mod solver;
