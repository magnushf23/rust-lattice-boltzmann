// Vertex, fragment shaders for simulation

// VERTEX STAGE

struct VertexInput {
    @location(0) vertex_pos: vec4<f32>,
    @location(1) tex_coord: vec2<f32>,
};

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tex_coord: vec2<f32>,
};

struct RenderParameters {
    imageSize: vec2<u32>
};

@group(0) @binding(0)
var<uniform> params: RenderParameters;

@vertex
fn vs_main(
    model: VertexInput,
) -> VertexOutput {
    var out: VertexOutput;
    out.clip_position = model.vertex_pos;
    out.tex_coord = model.tex_coord * vec2<f32>(params.imageSize);
    return out;
}


// FRAGMENT STAGE

@group(1) @binding(0)
var texture : texture_2d<f32>;
//@group(0) @binding(2)
//var<uniform> sampler : textureSample;

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    let loadCoord: vec2<i32> = vec2<i32>(in.tex_coord);
    return textureLoad(texture, loadCoord, 0);
}
