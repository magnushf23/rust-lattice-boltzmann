// COMPUTE shader

struct SimulationParameters {
    sizex : u32,
    sizey : u32,
    sizez : u32,
    timeStep : f32,
    relaxF : f32,
    relaxG : f32,
    beta : f32,
    bodyForcex : f32,
    bodyForcey : f32,
    bodyForcez : f32,
    k : f32,
    sim_time : f32,
};
struct Node {
    // Node Type: 0 - Fluid, 1 - Solid, 2 - Symmetric, 3 - Periodic
    nodeType: u32,
    // Index of source node
    sourceNodeIndex: array<u32,9>,
    // Index of source velocity
    sourceVelocityIndex: array<u32,9>,
    // First neighbor (for first order boundary conditions)
    firstOrderNeighborIndex: array<u32,9>,
};
struct LimitConditions {
    // Every u32 is unpacked to four 8-bit values representing: pressure, velocity, isothermal, flux conditions
    conditions: array<u32,9>,
    // Imposed temperature. Will hold imposed flux / velocity / pressure in future versions
    conditionsValues: array<f32,6>,
};
struct Mesh {
    nodes: array<Node>,
};
struct Limits {
    in: array<LimitConditions>,
};
struct Field {
    f: array<f32,9>,
    g: array<f32,9>,
    rho: f32,
    temperature: f32,
    velocity: vec2<f32>,
};
struct SimulationData {
    fields : array<Field>,
};

var<private> cvel : array<vec2<f32>,9> = array<vec2<f32>,9>(
    vec2<f32>(0.,0.),
    vec2<f32>(1.,0.),
    vec2<f32>(0.,1.),
    vec2<f32>(-1.,0.),
    vec2<f32>(0.,-1.),
    vec2<f32>(1.,1.),
    vec2<f32>(-1.,1.),
    vec2<f32>(-1.,-1.),
    vec2<f32>(1.,-1.),
);
var<private> cweights : array<f32,9> = array<f32,9>(
    0.4444444444444444444,
    0.1111111111111111111,
    0.1111111111111111111,
    0.1111111111111111111,
    0.1111111111111111111,
    0.0277777777777777778,
    0.0277777777777777778,
    0.0277777777777777778,
    0.0277777777777777778
);

@group(0) @binding(0)
var<uniform> params : SimulationParameters;
@group(0) @binding(1)
var<storage, read> mesh : Mesh;
@group(0) @binding(2)
var<storage, read> limits : Limits;
@group(0) @binding(3)
var<storage, read> dataA : SimulationData;
@group(0) @binding(4)
var<storage, read_write> dataB : SimulationData;
@group(1) @binding(0)
var textureOutput : texture_storage_2d<rgba8unorm, write>;

@compute @workgroup_size(8,8)
fn cs_main(@builtin(global_invocation_id) GlobalInvocationID : vec3<u32>) {
    // Domain size
    let size = vec3<u32>(params.sizex,params.sizey,params.sizez);
    // Flat 1D index of this invocation
    var index : u32 = GlobalInvocationID.x + GlobalInvocationID.y * size.x;

    // If solid, no update needed -> skip the node
    if(mesh.nodes[index].nodeType == 1u){
        var color : vec4<f32> = vec4<f32>(1.,1.,1.,1.);
        textureStore(textureOutput, vec2<i32>(i32(GlobalInvocationID.x), i32(GlobalInvocationID.y)), color);
        return;
    }

    // PART 1.a - Transfer fA to fB

    // Partial temperature known temperature components (for thermal boundary conditions)
    var partialTemperature = 0.;
    // Sum of the weights of the unknown temperature components
    var unknownTemperatureWeight = 0.;
    var partialVelocity = vec2<f32>(0., 0.);
    var unknownVelocityWeight = 0.;
    var partialRho = 0.;

    for(var q : u32 = 0u; q<9u; q = q + 1u){
        let sourceNode = mesh.nodes[index].sourceNodeIndex[q];
        let sourceVelocity = mesh.nodes[index].sourceVelocityIndex[q];
        // Transfer f and g distributions
        dataB.fields[index].f[q] = dataA.fields[sourceNode].f[sourceVelocity];
        dataB.fields[index].g[q] = dataA.fields[sourceNode].g[sourceVelocity];

        // For boundary conditions
        // Compute partial temperature on known components
        let conditions = unpack4x8unorm(limits.in[index].conditions[q]) * 255.;
        partialTemperature += dataB.fields[index].g[q] * (1. - max(conditions[2], conditions[3]));
        unknownTemperatureWeight += max(conditions[2], conditions[3]) * cweights[q];
        // Velocity
        let velocityUnknown = max(conditions[0], conditions[1]);
        partialVelocity += dataB.fields[index].f[q] / dataA.fields[index].rho * cvel[q] * (1. - velocityUnknown);
        partialRho += dataB.fields[index].f[q] * (1. - velocityUnknown);
        unknownVelocityWeight += velocityUnknown * cweights[q];
    }

    // PART 1.b - Compute fB components with Dirichlet/Neumann thermal condition

    // For Dirichlet (fixed Temperature)
    let wallTemperature = limits.in[index].conditionsValues[0];
    // For Neumann (fixed Flux)
    let fluxTemperature = dataA.fields[index].temperature + limits.in[index].conditionsValues[1];
    // Pressure imposed boundary (pressure is premultiplied with M and 1/R as P = P*M/R)
    let wallPressure = limits.in[index].conditionsValues[2];
    // Velocity imposed boundary
    let wallVelocity = vec3<f32>(
        limits.in[index].conditionsValues[3],
        limits.in[index].conditionsValues[4],
        limits.in[index].conditionsValues[5]
        );
    // Flow rate imposed. If this is the case, the target inflow value is written to
    // velocity condition vector.
    let flowRho = dataA.fields[index].rho + limits.in[index].conditionsValues[3];

    for(var q : u32 = 0u; q<9u; q = q + 1u){
        // Get first order neighbor (for correct temperature estimation in Half-Way scheme)
        let neighbor = mesh.nodes[index].firstOrderNeighborIndex[q];
        let neighborTemperature = dataA.fields[neighbor].temperature;
        let neighborVelocity = dot(dataA.fields[neighbor].velocity, cvel[q]);
        let neighborPressure = dataA.fields[neighbor].rho * neighborTemperature;
        let neighborRho = dataA.fields[neighbor].rho;
        let conditions = unpack4x8unorm(limits.in[index].conditions[q]) * 255.;

        // Update G value where needed
        if(conditions[2] == 1.) {
            // Dirichlet
            let targetTemperature = (2. * wallTemperature + neighborTemperature) / 3.;
            let value = (targetTemperature - partialTemperature) * cweights[q] / unknownTemperatureWeight;
            dataB.fields[index].g[q] = value;
        } else if(conditions[3] == 1.) {
            // Neumann
            let targetTemperature = (2. * fluxTemperature + neighborTemperature) / 3.;
            let value = (targetTemperature - partialTemperature) * cweights[q] / unknownTemperatureWeight;
            dataB.fields[index].g[q] = value;
        }

        // Update F value where needed
        if(conditions[0] == 1. && conditions[1] == 1.) {
            // Just like Neumann thermal flux condition, but with density
            let targetRho = (2. * flowRho + neighborRho) / 3.;
            let value = (targetRho - partialRho) * cweights[q] / unknownVelocityWeight;
            dataB.fields[index].f[q] = value;
        }
        else if(conditions[0] == 1.) {
            let targetTemperature = (2. * wallTemperature + neighborTemperature) / 3.;
            let targetPressure = (2. * wallPressure + neighborPressure) / 3.;
            let targetRho = targetPressure / targetTemperature;
            let value = (targetRho - partialRho) * cweights[q] / unknownVelocityWeight;
            dataB.fields[index].f[q] = max(value, 0.);
        } else if(conditions[1] == 1.) {
            // Velocity is 1D normal velocity for the moment (will be 2D : wallVelocity.xy)
            let targetVelocity = (2. * wallVelocity.x + neighborVelocity) / 3.;
            let value = (targetVelocity - dot(partialVelocity, cvel[q])) * cweights[q] / unknownVelocityWeight;
            dataB.fields[index].f[q] = value * dataA.fields[index].rho;
        }
    }

    // PART 2 - Compute moments

    var newRho : f32 = 0.;
    var newTemperature : f32 = 0.;
    var newVelocity : vec2<f32> = vec2<f32>(0.,0.);

    // First order moments
    for(var q : u32 = 0u; q<9u; q = q + 1u){
        newRho = newRho + dataB.fields[index].f[q];
        newTemperature = newTemperature + dataB.fields[index].g[q];
    }
    // Second order moments
    for(var q : u32 = 0u; q<9u; q = q + 1u){
        newVelocity = newVelocity + cvel[q] * dataB.fields[index].f[q] / newRho;
    }
    // Write fields
    dataB.fields[index].rho = newRho;
    dataB.fields[index].temperature = newTemperature;

    // Add body force (gravity) and buoyancy, which depends on local density fluctuation.
    // Local density fluctuation depends on thermal expansion of the fluid
    let temperatureVariation = (newTemperature - 1.);
    // Here the gravitational density gradient is removed for simplicity, but should be added back if needed ! -> ...*(1 - beta * dT)...
    let forceFactor = vec2<f32>(params.bodyForcex, params.bodyForcey) * ( - params.beta * temperatureVariation) * newRho;
    // Write real velocity
    dataB.fields[index].velocity = newVelocity + forceFactor / 2.;
    // Write equilibrium velocities
    let velFeq = newVelocity + forceFactor * params.relaxF;
    let velGeq = newVelocity + forceFactor * params.relaxG;

    // PART 3 - Collision

    // Square equilibrium (target) velocities
    let velFeq2 = dot(velFeq, velFeq);
    let velGeq2 = dot(velGeq, velGeq);

    // Compute for every component
    for(var q : u32 = 0u; q<9u; q = q + 1u){
        // F collision
        let ciVelFeq = dot(velFeq, cvel[q]);
        // Target mass distribution at equilibrium
        let feq = newRho * cweights[q] * (
            1.
            + ciVelFeq * params.k
            - velFeq2 * params.k / 2.
            + ciVelFeq * ciVelFeq * params.k * params.k / 2.);
        // Relaxation
        let fPost = dataB.fields[index].f[q] * (1. - 1./params.relaxF) + feq / params.relaxF;
        // Write
        dataB.fields[index].f[q] = fPost;

        // G collision
        let ciVelGeq = dot(velGeq, cvel[q]);
        // Target temperature distribution at equilibrium
        let geq = newTemperature * cweights[q] * (
            1.
            + ciVelGeq * params.k
            - velGeq2 * params.k / 2.
            + ciVelGeq * ciVelGeq * params.k * params.k / 2.);
        // Relaxation
        let gPost = dataB.fields[index].g[q] * (1. - 1./params.relaxG) + geq / params.relaxG;
        // Write
        dataB.fields[index].g[q] = gPost;
    }

    // DISPLAY

    let density = (newRho - 1.) * 5. + 0.5;
    let value = length(newVelocity + forceFactor / 2.) * 3.;
    //let value = density;
    let temp = (newTemperature - 1.) * 3.;

    // Schlieren of previous step (gradient computation requires all nodes to be processed)
    let right = mesh.nodes[index].sourceNodeIndex[1];
    let left = mesh.nodes[index].sourceNodeIndex[3];
    let up = mesh.nodes[index].sourceNodeIndex[2];
    let down = mesh.nodes[index].sourceNodeIndex[4];
    let drho = vec2<f32>(
        (dataA.fields[right].rho - dataA.fields[left].rho) / 2.,
        (dataA.fields[up].rho - dataA.fields[down].rho) / 2.);
    let schlieren = log(1. + dot(drho, drho) * 100000.);

    // If value is null, alpha is set to 0 to indicate error
    //var color : vec4<f32> = vec4<f32>(max(0., min(1., value * temp)), max(0., min(1., value)), max(0., min(1., value / temp)), 1.);
    var color  : vec4<f32> = vec4<f32>(temp, temp, temp, 1.);
    //var color  : vec4<f32> = vec4<f32>(schlieren, schlieren, schlieren, 1.);
    //let x = max(0., min(1., newVelocity.x * 5.));
    //let y = max(0., min(1., newVelocity.y * 5.));
    //var color : vec4<f32> = vec4<f32>(x/2. + 0.5, y/2. + 0.5, sqrt(1. - x*x - y*y)/2. + 0.5, 1.);
    //color = vec4<f32>(color.rgb * density, 1.);

    // Write color
    textureStore(textureOutput, vec2<i32>(i32(GlobalInvocationID.x), i32(GlobalInvocationID.y)), color);
}