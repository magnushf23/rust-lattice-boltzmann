#[repr(C)]
#[derive(Default, Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct RawD2Q9 {
    f: [f32; 9],
    g: [f32; 9],
    rho: f32,
    temperature: f32,
    vel_feq: [f32; 2],
    vel_geq: [f32; 2],
}

#[repr(C)]
#[derive(Default, Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct RawD3Q27 {
    f: [f32; 27],
    g: [f32; 27],
    rho: f32,
    temperature: f32,
    vel_feq: [f32; 3],
    vel_geq: [f32; 3],
}

#[repr(C)]
#[derive(Default, Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct RawLimits {
    pub conditions: [[u8; 4]; 9],
    pub conditions_values: [f32; 6],
}
