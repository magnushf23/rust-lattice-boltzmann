use itertools::Itertools;

use crate::dimension::LbmDimension;

/// Trace line inside a 2D domain
/// Algorithm from : https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
pub fn line_2d(x_1: i32, y_1: i32, x_2: i32, y_2: i32) -> Vec<[i32; 2]> {
    // Re-order points
    let (x1, y1, x2, y2) = match x_1 <= x_2 {
        true => (x_1, y_1, x_2, y_2),
        false => (x_2, y_2, x_1, y_1),
    };
    let mut dy = (y2 - y1) * 2;
    let dx = (x2 - x1) * 2;
    let mut step = 1;
    if dy < 0 {
        dy = -dy;
        step = -1;
    }

    // Case where |slope| > 1, flip x and y
    if dy > dx {
        return line_2d(y1, x1, y2, x2)
            .iter()
            .map(|[y, x]| [*x, *y])
            .collect::<Vec<[i32; 2]>>();
    }

    // Initialize
    let mut drawn_pixels = Vec::<[i32; 2]>::new();
    let mut error = dx;
    let mut y = y1;

    // Trace
    for x in x1..=x2 {
        drawn_pixels.push([x, y]);
        error -= dy;
        if error <= 0 {
            y += step;
            error += dx;
        }
    }
    drawn_pixels
}

/// Computes a 2D NACA 4-digit airfoil. The output is a vector of points
/// describing the border.
fn naca_4_geometry(resolution: usize, cord: f64, naca_code: [f64; 3]) -> Vec<[f64; 2]> {
    // Upper boundary
    let mut line_u = Vec::<[f64; 2]>::new();
    // Lower boundary
    let mut line_l = Vec::<[f64; 2]>::new();

    let m = naca_code[0] / 100.;
    let p = naca_code[1] / 10.;
    let t = naca_code[2] / 100.;

    let foil_thickness = |x: f64| -> f64 {
        let a = x / cord;
        t * cord / 0.2
            * (0.2969 * a.sqrt() - 0.126 * a - 0.3516 * a.powi(2) + 0.2843 * a.powi(3)
                - 0.1015 * a.powi(4))
    };

    let camber_line = |x: f64| -> f64 {
        if x >= 0. && x < p * cord {
            m * x / p.powi(2) * (2. * p - x / cord)
        } else if x >= p * cord && x <= cord {
            m * (cord - x) / (1. - p).powi(2) * (1. + x / cord - 2. * p)
        } else {
            0.
        }
    };

    let camber_derivative = |x: f64| -> f64 {
        if x >= 0. && x < p * cord {
            2. * m / p.powi(2) * (p - x / cord)
        } else if x >= p * cord && x <= cord {
            2. * m / (1. - p).powi(2) * (p - x / cord)
        } else {
            0.
        }
    };

    // Samples are taken linearly, with an extra point at 'i' = 0.25 for better
    // reconstruction of the leading edge
    let mut x_sampling = vec![0.; resolution];
    x_sampling[0] = 0.;
    x_sampling[1] = 0.25 * cord / (resolution - 2) as f64;
    for (i, x) in x_sampling.iter_mut().enumerate().skip(2) {
        *x = (i - 1) as f64 * cord / (resolution - 2) as f64;
    }

    for x in x_sampling {
        let y_t = foil_thickness(x);
        let y_c = camber_line(x);
        let dy_c = camber_derivative(x);
        let theta = dy_c.atan();
        let cos_t = theta.cos();
        let sin_t = theta.sin();

        // New points
        // Upper curve
        line_u.push([x - y_t * sin_t, y_c + y_t * cos_t]);
        // Lower curve
        line_l.push([x + y_t * sin_t, y_c - y_t * cos_t]);
    }

    // Combine upper and lower boundaries together and return
    line_l.reverse();
    [line_u, line_l].concat()
}

/// 2D flood fill algorithm for filling shapes drawn on a grid. In order to correctly fill shapes
/// with multiple convex hulls, the function instead fills the outside domain, and then returns the opposite
fn flood_fill_2d(
    dimension: LbmDimension,
    shape: Vec<[usize; 2]>,
    point_outside_shape: [usize; 2],
) -> Vec<[usize; 2]> {
    // Size of the desired grid
    let [max_x, max_y, _] = dimension.resolution_3d();
    let mut grid = vec![vec![0; max_y]; max_x];

    // First, draw the shape with value '1'
    for [x, y] in shape.iter() {
        grid[*x][*y] = 1;
    }

    // Start flood fill algorithm
    let mut queue = Vec::<[usize; 2]>::new();
    let mut filled_cells = Vec::<[usize; 2]>::new();
    // Start position must be outside of the shape !
    queue.push(point_outside_shape);

    while let Some(cell) = queue.pop() {
        // Get next cell in queue
        if cell[0] >= max_x || cell[1] >= max_y {
            continue;
        }
        if grid[cell[0]][cell[1]] == 0 {
            // If not marked, paint the cell with value '-1' (outside the shape)
            grid[cell[0]][cell[1]] = -1;
            // Explore neighbor cells
            queue.push([(cell[0] as i32 - 1).max(0) as usize, cell[1]]);
            queue.push([cell[0], (cell[1] as i32 - 1).max(0) as usize]);
            queue.push([cell[0] + 1, cell[1]]);
            queue.push([cell[0], cell[1] + 1]);
        }
    }
    // Collect the vector of cells inside the shape, ie: cells with value '0' or '1' (not '-1')
    for (x, row) in grid.iter().enumerate() {
        for (y, item) in row.iter().enumerate() {
            // Reject cells marked '-1'
            if *item != -1 {
                filled_cells.push([x, y]);
            }
        }
    }
    filled_cells
}

/// Computes a 2D NACA 4-digit airfoil. The output is a vector of points
/// on a 2D discrete grid.
pub fn naca_4_2d(
    // Domain dimensions
    dimension: LbmDimension,
    // x, y, and angle of attack
    center_position: [f64; 3],
    // Airfoil cord length
    cord: f64,
    // NACA 4-digit code
    naca_code: [f64; 3],
) -> Vec<[usize; 2]> {
    let x_step = dimension.voxel_size() as f64;
    let x_offset = center_position[0];
    let y_offset = center_position[1];
    let cos_angle = center_position[2].cos();
    let sin_angle = center_position[2].sin();

    // Resolution set to roughly one sample point per grid voxel
    let foil_resolution = (cord / x_step).floor() as usize;

    // Compute 2D NACA points
    let foil = naca_4_geometry(foil_resolution, cord, naca_code);

    // Trace a segment between every pair of points
    let mut drawn_voxels: Vec<[usize; 2]> = foil
        .iter()
        // Rotate and translate the foil to desire orientation and position
        .map(|[x, y]| {
            [
                (x - cord / 2.) * cos_angle - y * sin_angle + x_offset,
                (x - cord / 2.) * sin_angle + y * cos_angle + y_offset,
            ]
        })
        // Select every pair of adjacent points
        .circular_tuple_windows::<(_, _)>()
        // Flatten to a 1D Vec of i32 coordinates
        .flat_map(|(a, b)| {
            // Trace a segment between every pair
            line_2d(
                (a[0] / x_step).floor() as i32,
                (a[1] / x_step).floor() as i32,
                (b[0] / x_step).floor() as i32,
                (b[1] / x_step).floor() as i32,
            )
        })
        // Convert to usize by truncating negative coordinates to 0
        .map(|[x, y]| [x.max(0) as usize, y.max(0) as usize])
        .collect::<Vec<[usize; 2]>>();

    // Remove duplicates
    drawn_voxels.dedup();
    // Fill shape
    flood_fill_2d(dimension, drawn_voxels, [0, 0])
}
