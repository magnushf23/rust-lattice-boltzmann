use serde::{Deserialize, Serialize};

/// Dimension of the Lattice Boltzmann problem. D is the number of dimensions,
/// and Q the number of neighbors used for the velocity discretization.
#[derive(Debug, Copy, Clone, Deserialize, Serialize)]
pub enum LbmDimension {
    D2Q9 {
        // Number of voxels in each direction
        resolution: [usize; 2],
        // Size of the real domain
        real_size: [f32; 2],
    },
}
impl LbmDimension {
    // Velocity vectors for the 2D case (will be extended to larger neighborhood)
    pub const D2_VEL: [[i32; 2]; 9] = [
        [0, 0],
        [1, 0],
        [0, 1],
        [-1, 0],
        [0, -1],
        [1, 1],
        [-1, 1],
        [-1, -1],
        [1, -1],
    ];
    // Index of opposite velocity (will be extended to larger neighborhood)
    pub const D2_OPPOSITE_VEL: [usize; 9] = [0, 3, 4, 1, 2, 7, 8, 5, 6];
    // Weights for the D2Q9 scheme
    pub const D2Q9_WEIGHTS: [f32; 9] = [
        4. / 9.,
        1. / 9.,
        1. / 9.,
        1. / 9.,
        1. / 9.,
        1. / 36.,
        1. / 36.,
        1. / 36.,
        1. / 36.,
    ];
    // Return the size of the voxels
    pub fn voxel_size(&self) -> f32 {
        match self {
            LbmDimension::D2Q9 {
                resolution,
                real_size,
            } => real_size[0] / resolution[0] as f32,
        }
    }
    // Returns a string of the dimension type
    pub fn type_string(&self) -> &str {
        match self {
            LbmDimension::D2Q9 {
                resolution: _,
                real_size: _,
            } => "D2Q9",
        }
    }
    // Returns the 3D dimensions of the problem. For the 2D case, z resolution is set to 1.
    pub fn resolution_3d(&self) -> [usize; 3] {
        match self {
            LbmDimension::D2Q9 {
                resolution: size,
                real_size: _,
            } => [size[0], size[1], 1],
        }
    }
    // Returns the 3D dimensions of the problem. For the 2D case, z resolution is set to 1.
    pub fn resolution_u32(&self) -> [u32; 3] {
        let conversion_error_msg =
            "Created domain is too large to fit with an u32 index. Reduce the resolution.";
        match self {
            LbmDimension::D2Q9 {
                resolution: size,
                real_size: _,
            } => [
                u32::try_from(size[0]).expect(conversion_error_msg),
                u32::try_from(size[1]).expect(conversion_error_msg),
                1,
            ],
        }
    }
    // Returns the 3D real size of the problem. For the 2D case, z size is set to 0.
    pub fn real_size_3d(&self) -> [f32; 3] {
        match self {
            LbmDimension::D2Q9 {
                resolution: _,
                real_size: size,
            } => [size[0], size[1], 0.],
        }
    }
    // Total number of voxels in a domain
    pub fn node_number(&self) -> usize {
        match self {
            LbmDimension::D2Q9 {
                resolution: size,
                real_size: _,
            } => size[0] * size[1],
        }
    }
    // Volume of the real domain
    pub fn volume(&self) -> f32 {
        match self {
            LbmDimension::D2Q9 {
                resolution: _,
                real_size,
            } => real_size[0] * real_size[1],
        }
    }
    // Number of dimensions
    pub fn dimension(&self) -> usize {
        match self {
            LbmDimension::D2Q9 {
                resolution: _,
                real_size: _,
            } => 2,
        }
    }
    // Number of neighbors used for the velocity discretization
    pub fn neighborhood_size(&self) -> usize {
        match self {
            LbmDimension::D2Q9 {
                resolution: _,
                real_size: _,
            } => 9,
        }
    }
    // Returns a flattened 1D index of the voxels
    pub fn index(&self, xyz: [usize; 3]) -> usize {
        match self {
            LbmDimension::D2Q9 {
                resolution: size,
                real_size: _,
            } => xyz[0] + xyz[1] * size[0],
        }
    }
    // Neighbor of node 'id', in direction 'q'
    pub fn neighbor_position(&self, id: [usize; 3], q: usize) -> Option<[usize; 3]> {
        match self {
            LbmDimension::D2Q9 {
                resolution: size,
                real_size: _,
            } => {
                if q > 8 || id[0] >= size[0] || id[1] >= size[1] {
                    panic!("Index or velocity index is out of range");
                } else {
                    let res = [
                        id[0] as i32 + LbmDimension::D2_VEL[q][0],
                        id[1] as i32 + LbmDimension::D2_VEL[q][1],
                    ];
                    if res[0] < 0
                        || res[0] >= size[0] as i32
                        || res[1] < 0
                        || res[1] >= size[1] as i32
                    {
                        None
                    } else {
                        Some([res[0] as usize, res[1] as usize, 0])
                    }
                }
            }
        }
    }
    pub fn k_parameter(&self) -> f32 {
        match self {
            LbmDimension::D2Q9 {
                resolution: _,
                real_size: _,
            } => 3.,
        }
    }
    //pub fn iter2D(&self) -> _ {
    //    match self {
    //        LbmDimension::D2Q9 { size, real_size } => (0..size[0]).map(|x| (0..size[1]).map(move |y| (x,y))).flatten()
    //    }
    //}
}
