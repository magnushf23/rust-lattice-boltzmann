use crate::{
    dimension::LbmDimension, geometry::naca_4_2d, parameters::SimulationParameters,
    problem_definition::ProblemDefinition, raw::RawLimits, scene_object::SceneObject,
};
use core::fmt;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::cmp::min;

/// Node types currently implemented for the simulator :
///
/// * FLUID : The node represents a parcel of fluid
/// * SOLID : Solid node (with no-slip condition)
/// * SYMMETRIC : Solid node with slip (no friction)
/// * PERIODIC : Distributions are copied to the opposite side of the domain. Only for domain borders.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum NodeType {
    FLUID,
    SOLID,
    SYMMETRIC,
    PERIODIC,
}
impl NodeType {
    pub fn id(&self) -> usize {
        match self {
            NodeType::FLUID => 0,
            NodeType::SOLID => 1,
            NodeType::SYMMETRIC => 2,
            NodeType::PERIODIC => 3,
        }
    }
}
impl fmt::Display for NodeType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let name = match self {
            NodeType::FLUID => "FLUID",
            NodeType::SOLID => "SOLID",
            NodeType::SYMMETRIC => "SYMMETRIC",
            NodeType::PERIODIC => "PERIODIC",
        };
        write!(f, "{} (id: {})", name, self.id())
    }
}

/// Different thermal boundary conditions implemented in this project.
///
/// 3 types are available:
/// * NONE: No special treatment is applied. For fluid nodes or adiabatic walls
/// * TEMPERATURE: Imposed temperature (K) at solid boundary.
/// * FLUX: Fixed incoming energy flux (W/m^2). Wall thermal conductivity should also be specified.
#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub enum ThermalCondition {
    NONE,
    TEMPERATURE { temperature: f32 },
    FLUX { flux: f32, wall_conductivity: f32 },
}

/// Boundary condition types for inlet / outlet flows
///
/// * NONE: No special treatment is applied.
/// * PRESSURE: Pressure is fixed.
/// * VELOCITY: 3D velocity vector is fixed. Works both as inlet and outlet.
/// * COMBINED: Both pressure and velocity are imposed.
#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub enum FlowCondition {
    NONE,
    PRESSURE { pressure: f32 },
    VELOCITY { velocity: [f32; 3] },
    FLOW { pressure: f32, flow_rate: f32 },
}

/// SimulationBuilder is the main struct used for building simulation contexts and initializing fields.
/// The context can then be exported to raw data and mesh types, to be sent to the GPU solver.
/// Use new() to create an empty domain with homogeneous initial density and temperature,
/// and solid no-slip borders.
pub struct SimulationBuilder {
    // Domain dimension, size and resolution
    pub dimension: LbmDimension,
    // Simulation parameters
    parameters: SimulationParameters,
    // 0 - fluid, 1 - solid no-slip, 2 - symmetric, 3 - periodic
    node_type: Vec<NodeType>,
    // Source node of a given node for a given velocity
    src_node_index: Vec<Vec<usize>>,
    // Source velocity of a given node for a given velocity
    src_vel_index: Vec<Vec<usize>>,
    // First neighbor
    first_order_neighbor: Vec<Vec<usize>>,
    // Weights for velocities where pressure inlet condition has to be computed
    pressure_condition: Vec<Vec<f32>>,
    // Weights for velocities where velocity inlet condition has to be computed
    velocity_condition: Vec<Vec<f32>>,
    // Weights for velocities where thermal Dirichlet condition has to be computed
    isothermal_condition: Vec<Vec<f32>>,
    // Weights for velocities where thermal Neumann condition has to be computed
    flux_condition: Vec<Vec<f32>>,
    // Input values for border conditions. Temperature, Flux, Pressure, Velocity xyz
    conditions_values: Vec<[f32; 6]>,
    // Mass distribution
    f: Vec<Vec<f32>>,
    // Thermal energy distribution
    g: Vec<Vec<f32>>,
    // Density field
    rho: Vec<f32>,
    // Temperature field
    temperature: Vec<f32>,
    // Velocity field
    velocity: Vec<Vec<f32>>,
    // Are the fields correctly initialized ?
    is_initialized: bool,
}

impl SimulationBuilder {
    // Create an empty domain with homogeneous initial density and temperature, and solid no-slip borders.
    pub fn new(parameters: SimulationParameters) -> Self {
        let dim = parameters.dimension;
        let length = dim.node_number();
        let max_q = dim.neighborhood_size();

        // Initialize mesh attributes
        let mut node_type = vec![NodeType::FLUID; length];
        let src_node_index: Vec<Vec<usize>>;
        let src_vel_index: Vec<Vec<usize>>;
        let first_order_neighbor = vec![vec![0; max_q]; length];
        let pressure_condition = vec![vec![0.; max_q]; length];
        let velocity_condition = vec![vec![0.; max_q]; length];
        let isothermal_condition = vec![vec![0.; max_q]; length];
        let flux_condition = vec![vec![0.; max_q]; length];
        let conditions_values = vec![[0.; 6]; length];

        // Initialize fields
        let f = vec![vec![0.; max_q]; length];
        let g = vec![vec![0.; max_q]; length];
        let rho = vec![1.; length];
        let temperature = vec![1.; length];
        let velocity = vec![vec![0.; dim.dimension()]; length];

        // Construct
        match dim {
            LbmDimension::D2Q9 {
                resolution: size,
                real_size: _,
            } => {
                // Set node types : 0 (fluid), 1 (solid no slip), 2 (solid slip), 3 (periodic)
                for x in 0..size[0] {
                    node_type[dim.index([x, 0, 0])] = NodeType::SOLID;
                    node_type[dim.index([x, size[1] - 1, 0])] = NodeType::SOLID;
                }
                for y in 0..size[1] {
                    node_type[dim.index([0, y, 0])] = NodeType::SOLID;
                    node_type[dim.index([size[0] - 1, y, 0])] = NodeType::SOLID;
                }

                // Configure neighborhood
                (src_node_index, src_vel_index) = SimulationBuilder::get_sources(&dim, &node_type);
            }
        }

        let mut this_instance = SimulationBuilder {
            dimension: dim,
            parameters,
            node_type,
            src_node_index,
            src_vel_index,
            first_order_neighbor,
            pressure_condition,
            velocity_condition,
            isothermal_condition,
            flux_condition,
            conditions_values,
            f,
            g,
            rho,
            temperature,
            velocity,
            is_initialized: false,
        };

        this_instance.contruct_neighborhood();
        this_instance
    }

    fn get_sources(
        dim: &LbmDimension,
        node_type: &[NodeType],
    ) -> (Vec<Vec<usize>>, Vec<Vec<usize>>) {
        let length = dim.node_number();
        let max_q = dim.neighborhood_size();
        // Output arrays
        let mut src_node_index = vec![vec![0_usize; max_q]; length];
        let mut src_vel_index = vec![vec![0_usize; max_q]; length];
        // Array for asserting that the sources are well constructed
        let mut validation_vector = vec![vec![0_usize; max_q]; length];

        let size = dim.resolution_3d();

        let iterator_3d = (0..size[0])
            .flat_map(|x| (0..size[1]).flat_map(move |y| (0..size[2]).map(move |z| (x, y, z))));
        for (x, y, z) in iterator_3d {
            let current_3d_id = [x, y, z];
            let current_index = dim.index(current_3d_id);
            src_node_index[current_index][0] = current_index;
            src_vel_index[current_index][0] = 0;

            match node_type[current_index] {
                NodeType::FLUID => {
                    for q in 1..max_q {
                        let source_node = dim.neighbor_position(current_3d_id, q);
                        let source_node_index = match source_node {
                            Some(position) => dim.index(position),
                            None => panic!("Fluid node misses neighbor(s)"),
                        };
                        match node_type[source_node_index] {
                            NodeType::FLUID => {
                                src_node_index[source_node_index][q] = current_index;
                                src_vel_index[source_node_index][q] = q;
                            }
                            NodeType::SOLID => {
                                // Do nothing (case will be handled by the solid node)
                            }
                            other => panic!("Node type {other} not implemented yet"),
                        }
                    }
                }
                NodeType::SOLID => {
                    // SOLID NO SLIP
                    for q in 1..max_q {
                        // Solid node is its own source
                        src_node_index[current_index][q] = current_index;
                        src_vel_index[current_index][q] = q;

                        // Bounce back operation on neighbor nodes TODO
                        let source_node = dim.neighbor_position(current_3d_id, q);
                        let source_node_index = match source_node {
                            Some(position) => dim.index(position),
                            None => continue, // No neighbor node to bounce back to
                        };
                        // If fluid, the neighbor node sends fluid to itself with opposite velocity
                        if node_type[source_node_index] == NodeType::FLUID {
                            src_node_index[source_node_index][q] = source_node_index;
                            src_vel_index[source_node_index][q] = LbmDimension::D2_OPPOSITE_VEL[q];
                        }
                    }
                }
                other => panic!("Node type {other} not implemented yet"),
            }
        }

        // TESTING
        for (n, q) in (0..length).flat_map(|n| (0..max_q).map(move |q| (n, q))) {
            let source_node = src_node_index[n][q];
            let source_vel = src_vel_index[n][q];
            validation_vector[source_node][source_vel] += 1;
        }

        for value in validation_vector.iter().flatten() {
            assert_eq!(*value, 1, "Problem during configuration of node and velocity sources. Found value '{}' (should be 1).", *value);
        }

        (src_node_index, src_vel_index)
    }

    /// This function determines the first order (direct) neighbor for the nodes that have special
    /// boundary conditions. This is used for making correct estimation of temperature and other
    /// quantities when working with the Half-Way bounce scheme.
    ///
    /// The function configures the `first_order_neighbor` vec.
    fn contruct_neighborhood(&mut self) {
        let size = self.dimension.resolution_3d();

        // Loop over all cells
        (0..size[0])
            .cartesian_product(0..size[1])
            .cartesian_product(0..size[2])
            .map(|((x, y), z)| [x, y, z])
            .for_each(|coordinates| {
                let index = self.dimension.index(coordinates);

                if self.node_type[index] == NodeType::SOLID {
                    // For each solid cell, look for fluid neighbor cells
                    for q in 0..self.dimension.neighborhood_size() {
                        let fluid_cell = match self.dimension.neighbor_position(coordinates, q) {
                            Some(position) => position,
                            None => continue,
                        };
                        let fluid_cell_index = self.dimension.index(fluid_cell);
                        // Only fluid cells (type 0, 3) are considered
                        match self.node_type[fluid_cell_index] {
                            NodeType::FLUID => {}
                            NodeType::SOLID => continue,
                            NodeType::SYMMETRIC => continue,
                            NodeType::PERIODIC => todo!(), // Change this when type 3 is implemented
                        };
                        // We search for the fluid cell's direct neighbor following direction q
                        let first_neighbor = match self.dimension.neighbor_position(fluid_cell, q) {
                            Some(position) => self.dimension.index(position),
                            None => panic!(
                                "Fluid cell {:?} has no direct neighbor in direction {}.",
                                fluid_cell, q
                            ),
                        };
                        // Write
                        self.first_order_neighbor[fluid_cell_index][q] = first_neighbor;
                    }
                }
            });
    }

    pub fn fill_node_list(
        &mut self,
        node_coordinates: Vec<[usize; 3]>,
        new_node_type: NodeType,
        thermal_condition: ThermalCondition,
        flow_condition: FlowCondition,
    ) {
        // Set node types to 1 (solid)
        node_coordinates.iter().for_each(|node_id| {
            self.node_type[self.dimension.index(*node_id)] = new_node_type;
            self.set_thermal_condition(*node_id, thermal_condition);
            self.set_flow_condition(*node_id, flow_condition)
        });

        // Update "source node indices"
        (self.src_node_index, self.src_vel_index) =
            SimulationBuilder::get_sources(&self.dimension, &self.node_type);
        self.contruct_neighborhood();
        self.is_initialized = false;
    }

    // Set input flow condition on a given list of nodes
    pub fn set_flow_condition(
        &mut self,
        node_coordinates: [usize; 3],
        flow_condition: FlowCondition,
    ) {
        let ref_velocity = self.parameters.get_dimension_factor([0, 1, -1, 0]) as f32;
        let rs = (self.parameters.fluid.press_0
            / self.parameters.fluid.rho_0
            / self.parameters.fluid.temp_0) as f32;
        let ref_pm_r = self.parameters.get_dimension_factor([1, -3, 0, 1]) as f32;
        let ref_flow_rate = self.parameters.get_dimension_factor([1, -2, -1, 0]) as f32;

        for q in 0..self.dimension.neighborhood_size() {
            // Set neighbor node
            let neighbor = match self.dimension.neighbor_position(node_coordinates, q) {
                Some(position) => self.dimension.index(position),
                None => continue,
            };

            match flow_condition {
                FlowCondition::NONE => (),
                FlowCondition::PRESSURE { pressure } => {
                    self.pressure_condition[neighbor][q] = 1.;
                    self.conditions_values[neighbor][2] = pressure / rs / ref_pm_r;
                }
                FlowCondition::VELOCITY { velocity } => {
                    self.velocity_condition[neighbor][q] = 1.;
                    (0..3).zip(velocity).for_each(|(i, v_i)| {
                        self.conditions_values[neighbor][i + 3] = v_i / ref_velocity;
                    });
                }
                FlowCondition::FLOW {
                    pressure,
                    flow_rate,
                } => {
                    self.pressure_condition[neighbor][q] = 1.;
                    self.velocity_condition[neighbor][q] = 1.;
                    self.conditions_values[neighbor][2] = pressure / rs / ref_pm_r;
                    for i in 0..3 {
                        self.conditions_values[neighbor][i + 3] = flow_rate / ref_flow_rate;
                    }
                }
            }
        }
    }
    // Set thermal condition on a given list of nodes
    pub fn set_thermal_condition(
        &mut self,
        node_coordinates: [usize; 3],
        thermal_condition: ThermalCondition,
    ) {
        let ref_temperature = self.parameters.get_dimension_factor([0, 0, 0, 1]) as f32;
        let flux_factor = self.parameters.get_dimension_factor([0, 1, 0, -1]) as f32;
        for q in 0..self.dimension.neighborhood_size() {
            // Set neighbor node
            let neighbor = match self.dimension.neighbor_position(node_coordinates, q) {
                Some(position) => self.dimension.index(position),
                None => continue,
            };

            match thermal_condition {
                ThermalCondition::NONE => (),
                ThermalCondition::TEMPERATURE { temperature } => {
                    self.isothermal_condition[neighbor][q] = 1.;
                    self.conditions_values[neighbor][0] = temperature / ref_temperature;
                }
                ThermalCondition::FLUX {
                    flux,
                    wall_conductivity,
                } => {
                    self.flux_condition[neighbor][q] = 1.;
                    self.conditions_values[neighbor][1] =
                        flux * flux_factor / wall_conductivity / 2.;
                }
            }
        }
    }

    // Fill a region with a certain node type. Specify region with the 3D ranges [min, max]
    pub fn fill_cube(
        &mut self,
        mut ranges: [[usize; 2]; 3],
        new_node_type: NodeType,
        thermal_condition: ThermalCondition,
        flow_condition: FlowCondition,
    ) {
        // Limit ranges to the domain
        let limits = self.dimension.resolution_3d();
        ranges.iter_mut().zip(limits).for_each(|([a, b], lim)| {
            *a = min(*a, lim);
            *b = min(*b, lim);
        });

        // Set nodes to 'new_node_type'
        (ranges[0][0]..ranges[0][1])
            .cartesian_product(ranges[1][0]..ranges[1][1])
            .cartesian_product(ranges[2][0]..ranges[2][1])
            .map(|((x, y), z)| [x, y, z])
            .for_each(|coordinates| {
                self.node_type[self.dimension.index(coordinates)] = new_node_type;
                self.set_thermal_condition(coordinates, thermal_condition);
                self.set_flow_condition(coordinates, flow_condition);
            });

        // Update "source node indices"
        (self.src_node_index, self.src_vel_index) =
            SimulationBuilder::get_sources(&self.dimension, &self.node_type);
        self.contruct_neighborhood();
        self.is_initialized = false;
    }
    /*
        pub fn fill_ball(
            &mut self,
            center: [f64; 3],
            radius: f64,
            new_node_type: NodeType,
            thermal_condition: ThermalCondition,
            flow_condition: FlowCondition,
        ) {
            let outline: [[f64; 2]; 3] = [
                [center[0] - radius, center[0] + radius],
                [center[1] - radius, center[1] + radius],
                [center[2] - radius, center[2] + radius],
            ];

            let real_size = self.dimension.real_size_3d();
            let limits = self.dimension.resolution_3d();
            let mut ranges = [
                [
                    (outline[0][0] as f32 / real_size[0] * limits[0] as f32).floor() as usize,
                    (outline[0][1] as f32 / real_size[0] * limits[0] as f32).floor() as usize,
                ],
                [
                    (outline[1][0] as f32 / real_size[1] * limits[0] as f32).floor() as usize,
                    (outline[1][1] as f32 / real_size[1] * limits[0] as f32).floor() as usize,
                ],
                [
                    (outline[2][0] as f32 / real_size[2] * limits[0] as f32).floor() as usize,
                    (outline[2][1] as f32 / real_size[2] * limits[0] as f32).floor() as usize,
                ],
            ];

            // Limit ranges to the domain
            let limits = self.dimension.resolution_3d();
            ranges.iter_mut().zip(limits).for_each(|([a, b], lim)| {
                *a = min(*a, lim);
                *b = min(*b, lim);
            });

            // Set nodes to 'new_node_type'
            (ranges[0][0]..ranges[0][1])
                .cartesian_product(ranges[1][0]..ranges[1][1])
                .cartesian_product(ranges[2][0]..ranges[2][1])
                .map(|((x, y), z)| [x, y, z])
                .for_each(|coordinates| {
                    let dist = (coordinates.iter().zip(center).map(|i, cx|
                         (i/)
                    ).sum::<usize>() as f64).sqrt();

                    self.node_type[self.dimension.index(coordinates)] = new_node_type;
                    self.set_thermal_condition(coordinates, thermal_condition);
                    self.set_flow_condition(coordinates, flow_condition);
                });

            // Update "source node indices"
            (self.src_node_index, self.src_vel_index) =
                SimulationBuilder::get_sources(&self.dimension, &self.node_type);
            self.contruct_neighborhood();
            self.is_initialized = false;
        }
    */
    pub fn initialize_fields(
        &mut self,
        initial_rho: f32,
        initial_temperature: f32,
        initial_velocity: [f32; 3],
    ) {
        let max_q = self.dimension.neighborhood_size();
        let initial_rho_dimensionless =
            initial_rho / self.parameters.get_dimension_factor([1, -3, 0, 0]) as f32;
        let initial_temp_dimensionless =
            initial_temperature / self.parameters.get_dimension_factor([0, 0, 0, 1]) as f32;
        let velocity_dimension = self.parameters.get_dimension_factor([0, 1, -1, 0]) as f32;
        dbg!(initial_temp_dimensionless);
        match self.dimension {
            LbmDimension::D2Q9 {
                resolution: _,
                real_size: _,
            } => {
                let new_velocity = [
                    initial_velocity[0] / velocity_dimension,
                    initial_velocity[1] / velocity_dimension,
                ];
                let k = self.dimension.k_parameter();
                let velfeq2 = new_velocity[0].powi(2) + new_velocity[1].powi(2);
                // Fill field arrays
                for i in 0..self.dimension.node_number() {
                    match self.node_type[i] {
                        NodeType::FLUID => {
                            self.rho[i] = initial_rho_dimensionless;
                            self.temperature[i] = initial_temp_dimensionless;

                            for q in 0..max_q {
                                let civelfeq = new_velocity[0]
                                    * (LbmDimension::D2_VEL[q][0] as f32)
                                    + new_velocity[1] * (LbmDimension::D2_VEL[q][1] as f32);
                                let distribution = 1. + civelfeq * k - velfeq2 * k / 2.
                                    + civelfeq * civelfeq * k * k / 2.;
                                self.f[i][q] = initial_rho_dimensionless
                                    * LbmDimension::D2Q9_WEIGHTS[q]
                                    * distribution;
                                self.g[i][q] = initial_temp_dimensionless
                                    * LbmDimension::D2Q9_WEIGHTS[q]
                                    * distribution;
                            }
                        }
                        _ => {
                            self.rho[i] = 1.;
                            self.temperature[i] = initial_temp_dimensionless;

                            for q in 0..max_q {
                                self.f[i][q] =
                                    initial_rho_dimensionless * LbmDimension::D2Q9_WEIGHTS[q];
                                self.g[i][q] =
                                    initial_temp_dimensionless * LbmDimension::D2Q9_WEIGHTS[q];
                            }
                        }
                    }
                }
            }
        }
        self.is_initialized = true;
    }

    pub fn get_raw_data(&self) -> Result<Vec<f32>, &'static str> {
        match self.is_initialized {
            true => Ok(self
                .f
                .iter()
                .zip(self.g.iter())
                .zip(self.rho.iter())
                .zip(self.temperature.iter())
                .zip(self.velocity.iter())
                .flat_map(|((((f_i, g_i), rho_i), temp_i), vel_i)| {
                    let mut res = Vec::new();
                    res.extend(f_i);
                    res.extend(g_i);
                    res.push(*rho_i);
                    res.push(*temp_i);
                    res.extend(vel_i);
                    res
                })
                .collect()),
            false => Err("Fields not initialized yet"),
        }
    }

    pub fn get_raw_mesh(&self) -> Vec<u32> {
        self.node_type
            .iter()
            .zip(self.src_node_index.iter())
            .zip(self.src_vel_index.iter())
            .zip(self.first_order_neighbor.iter())
            .flat_map(|(((nt_i, sni_i), svi_i), fon_i)| {
                let mut res = Vec::new();
                res.push(nt_i.id());
                res.extend(sni_i);
                res.extend(svi_i);
                res.extend(fon_i);
                res
            })
            .map(|e| {
                u32::try_from(e).expect(
                    "Created domain is too large to fit with an u32 index. Reduce the resolution.",
                )
            })
            .collect()
    }

    /// Exports boundary condition data to raw buffer data (for GPU)
    pub fn get_raw_limits(&self) -> Vec<RawLimits> {
        self.pressure_condition
            .iter()
            .zip(self.velocity_condition.iter())
            .zip(self.isothermal_condition.iter())
            .zip(self.flux_condition.iter())
            .map(|(((press_i, vel_i), isot_i), flux_i)| {
                let array: [[u8; 4]; 9] = press_i
                    .iter()
                    .zip(vel_i)
                    .zip(isot_i)
                    .zip(flux_i)
                    .map(|(((p, v), i), f)| [*p as u8, *v as u8, *i as u8, *f as u8])
                    .collect::<Vec<[u8; 4]>>()
                    .try_into()
                    .unwrap();
                array
            })
            .zip(self.conditions_values.iter())
            .map(|(conditions, values)| RawLimits {
                conditions,
                conditions_values: *values,
            })
            .collect::<Vec<RawLimits>>()
    }

    pub fn from_problem_definition(problem: &ProblemDefinition) -> Self {
        let mut builder = SimulationBuilder::new(problem.sim_parameters);

        // Add the scene objects to the scene
        for object in &problem.scene_objects {
            match object {
                SceneObject::Cube {
                    label: _,
                    ranges,
                    node_type,
                    thermal_condition,
                    flow_condition,
                } => {
                    builder.fill_cube(*ranges, *node_type, *thermal_condition, *flow_condition);
                }
                SceneObject::Airfoil {
                    label: _,
                    center_position,
                    cord,
                    naca_code,
                    thermal_condition,
                } => {
                    let airfoil = naca_4_2d(
                        problem.sim_parameters.dimension,
                        *center_position,
                        *cord,
                        *naca_code,
                    )
                    .iter()
                    .map(|cell| [cell[0], cell[1], 0])
                    .collect();

                    builder.fill_node_list(
                        airfoil,
                        NodeType::SOLID,
                        *thermal_condition,
                        FlowCondition::NONE,
                    );
                }
                SceneObject::Ball {
                    label: _,
                    center: _,
                    radius: _,
                    thermal_condition: _,
                    flow_condition: _,
                } => {
                    todo!();
                }
            }
        }

        builder.initialize_fields(
            problem.initial_rho,
            problem.initial_temperature,
            problem.initial_velocity,
        );
        builder
    }
}
