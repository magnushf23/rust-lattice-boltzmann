use std::mem;

use wgpu::util::DeviceExt;

use crate::{
    dimension::LbmDimension, gpu::Gpu, imagetexture::ImageTexture,
    parameters::RawSimulationParameters, raw::RawLimits, simulationbuilder::SimulationBuilder,
};

const WORKGROUP_SIZE: (u32, u32, u32) = (8, 8, 8);

pub struct GpuSolver {
    pub params: RawSimulationParameters,
    pub sim_parameters_buffer: wgpu::Buffer,
    pub mesh_buffer: wgpu::Buffer,
    pub limits_buffer: wgpu::Buffer,
    pub data_buffers: Vec<wgpu::Buffer>,
    pub data_bind_groups: Vec<wgpu::BindGroup>,
    pub compute_pipeline: wgpu::ComputePipeline,
    pub work_group_count: [u32; 3],
    pub frame_num: usize,
}

impl GpuSolver {
    pub fn new(
        gpu: &Gpu,
        builder: &SimulationBuilder,
        params: &RawSimulationParameters,
        texture: &ImageTexture,
    ) -> Self {
        let shader_text = match builder.dimension {
            LbmDimension::D2Q9 {
                resolution: _,
                real_size: _,
            } => include_str!("shaders/computeD2Q9_smagorinsky.wgsl"),
        };
        let compute_shader = gpu
            .device
            .create_shader_module(wgpu::ShaderModuleDescriptor {
                label: Some("Compute shader"),
                source: wgpu::ShaderSource::Wgsl(shader_text.into()),
            });

        // Fields (f, g, density, temperature, velocity)
        let raw_data = builder.get_raw_data().unwrap();
        // Mesh (node type, source nodes + velocities)
        let raw_mesh = builder.get_raw_mesh();
        // Boundary conditions
        let raw_limits = builder.get_raw_limits();

        // Buffers
        let sim_parameters_buffer =
            gpu.device
                .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    label: Some("Sim Parameters Buffer"),
                    contents: bytemuck::cast_slice(&[*params]),
                    usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
                });

        let mesh_buffer = gpu
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Mesh Buffer"),
                contents: bytemuck::cast_slice(&raw_mesh),
                usage: wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
            });
        let limits_buffer = gpu
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Limits Buffer"),
                contents: bytemuck::cast_slice(&raw_limits),
                usage: wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
            });

        let mut data_buffers = Vec::<wgpu::Buffer>::new();
        for i in 0..2 {
            data_buffers.push(
                gpu.device
                    .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                        label: Some(&format!("Data Buffer {}", i)),
                        contents: bytemuck::cast_slice(&raw_data),
                        usage: wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
                    }),
            );
        }

        let compute_bind_group_layout =
            gpu.device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    entries: &[
                        // Binding 0 --> Sim Parameters Buffer
                        wgpu::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: wgpu::ShaderStages::COMPUTE,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Uniform,
                                has_dynamic_offset: false,
                                min_binding_size: wgpu::BufferSize::new(mem::size_of::<
                                    RawSimulationParameters,
                                >(
                                )
                                    as u64),
                            },
                            count: None,
                        },
                        // Binding 1 --> Mesh Buffer
                        wgpu::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: wgpu::ShaderStages::COMPUTE,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Storage { read_only: true },
                                has_dynamic_offset: false,
                                min_binding_size: wgpu::BufferSize::new(
                                    (mem::size_of::<u32>() * raw_mesh.len()) as u64,
                                ),
                            },
                            count: None,
                        }, // Binding 2 --> Limits Buffer
                        wgpu::BindGroupLayoutEntry {
                            binding: 2,
                            visibility: wgpu::ShaderStages::COMPUTE,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Storage { read_only: true },
                                has_dynamic_offset: false,
                                min_binding_size: wgpu::BufferSize::new(
                                    (mem::size_of::<RawLimits>() * raw_limits.len()) as u64,
                                ),
                            },
                            count: None,
                        },
                        // Binding 3 --> Data read buffer
                        wgpu::BindGroupLayoutEntry {
                            binding: 3,
                            visibility: wgpu::ShaderStages::COMPUTE,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Storage { read_only: true },
                                has_dynamic_offset: false,
                                min_binding_size: wgpu::BufferSize::new(
                                    (mem::size_of::<f32>() * raw_data.len()) as u64,
                                ),
                            },
                            count: None,
                        },
                        // Binding 4 --> Data read & write buffer
                        wgpu::BindGroupLayoutEntry {
                            binding: 4,
                            visibility: wgpu::ShaderStages::COMPUTE,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Storage { read_only: false },
                                has_dynamic_offset: false,
                                min_binding_size: wgpu::BufferSize::new(
                                    (mem::size_of::<f32>() * raw_data.len()) as u64,
                                ),
                            },
                            count: None,
                        },
                    ],
                    label: None,
                });

        let compute_pipeline_layout =
            gpu.device
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("Compute layout"),
                    bind_group_layouts: &[
                        &compute_bind_group_layout,
                        &texture.compute_bind_group_layout,
                    ],
                    push_constant_ranges: &[],
                });

        let compute_pipeline =
            gpu.device
                .create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
                    label: Some("Compute pipeline"),
                    layout: Some(&compute_pipeline_layout),
                    module: &compute_shader,
                    entry_point: "cs_main",
                });

        let mut data_bind_groups = Vec::<wgpu::BindGroup>::new();
        for i in 0..2 {
            data_bind_groups.push(gpu.device.create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &compute_bind_group_layout,
                entries: &[
                    // First binding --> Simulation parameters
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: sim_parameters_buffer.as_entire_binding(),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: mesh_buffer.as_entire_binding(),
                    },
                    wgpu::BindGroupEntry {
                        binding: 2,
                        resource: limits_buffer.as_entire_binding(),
                    },
                    wgpu::BindGroupEntry {
                        binding: 3,
                        resource: data_buffers[i % 2].as_entire_binding(),
                    },
                    wgpu::BindGroupEntry {
                        binding: 4,
                        resource: data_buffers[(i + 1) % 2].as_entire_binding(),
                    },
                ],
                label: None,
            }));
        }

        let work_group_count = [
            ((builder.dimension.resolution_3d()[0] as f32) / (WORKGROUP_SIZE.0 as f32)).ceil()
                as u32,
            ((builder.dimension.resolution_3d()[1] as f32) / (WORKGROUP_SIZE.1 as f32)).ceil()
                as u32,
            ((builder.dimension.resolution_3d()[2] as f32) / (WORKGROUP_SIZE.2 as f32)).ceil()
                as u32,
        ];

        GpuSolver {
            params: *params,
            sim_parameters_buffer,
            mesh_buffer,
            limits_buffer,
            data_buffers,
            data_bind_groups,
            compute_pipeline,
            work_group_count,
            frame_num: 0,
        }
    }

    pub fn next_step(
        &mut self,
        queue: &wgpu::Queue,
        encoder: &mut wgpu::CommandEncoder,
        output_bind_group: &wgpu::BindGroup,
    ) {
        // Update time
        self.params.sim_time = self.params.time_step * self.frame_num as f32;
        queue.write_buffer(
            &self.sim_parameters_buffer,
            0,
            bytemuck::cast_slice(&[self.params]),
        );

        // Advance one step
        let mut cpass = encoder.begin_compute_pass(&wgpu::ComputePassDescriptor {
            label: Some("Simulation step"),
        });

        cpass.set_pipeline(&self.compute_pipeline);
        cpass.set_bind_group(0, &self.data_bind_groups[self.frame_num % 2], &[]);
        cpass.set_bind_group(1, output_bind_group, &[]);
        cpass.dispatch_workgroups(
            self.work_group_count[0],
            self.work_group_count[1],
            self.work_group_count[2],
        );

        self.frame_num += 1;
    }

    pub fn current_simulation_time(&self) -> f64 {
        self.frame_num as f64 * self.params.time_step as f64
    }
}
