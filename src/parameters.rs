use serde::{Deserialize, Serialize};

use crate::dimension::LbmDimension;

/// Physical parameters for the fluid
#[derive(Debug, Copy, Clone, Deserialize, Serialize)]
pub struct Fluid {
    /// Reference density
    pub rho_0: f64,
    /// Reference temperature
    pub temp_0: f64,
    /// Reference pressure
    pub press_0: f64,
    /// Dynamic viscosity
    pub mu: f64,
    /// Kinematic viscosity
    pub nu: f64,
    /// Thermal diffusivity
    pub kappa: f64,
    /// Isobaric volumic coefficient of thermal expansion
    pub beta: f64,
    /// Heat capacity per mass unit
    pub cp: f64,
    /// Adiabatic coefficient
    pub gamma: f64,
    /// Adiabatic sound velocity
    pub c_0: f64,
}
impl Fluid {
    /// Initialize fluid with parameters for air at 1 bar
    pub fn air() -> Self {
        Fluid {
            rho_0: 1.225,
            temp_0: 300.,
            press_0: 1e5,
            mu: 1.96e-5,
            nu: 1.6e-5,
            kappa: 2.3e-5,
            beta: 3.4e-3,
            cp: 1005.,
            gamma: 1.4,
            c_0: 343.,
        }
    }
    /// Initialize fluid with parameters for water at 1 bar
    pub fn water() -> Self {
        Fluid {
            rho_0: 997.,
            temp_0: 300.,
            press_0: 1e5,
            mu: 0.89e-3,
            nu: 0.8626e-6,
            kappa: 1.48e-7,
            beta: 210e-6,
            cp: 4184.,
            gamma: 1.4,
            c_0: 1480.,
        }
    }
}

/// Set of parameters used by the Lattice Boltzmann algorithm
#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct SimulationParameters {
    /// Dimension of the problem
    pub dimension: LbmDimension,
    /// Spatial step
    delta_x: f64,
    /// Time step
    delta_t: f64,
    /// Characteristic mass (for conversion to dimensionless quantities)
    characteristic_mass: f64,
    /// Characteristic length (for dimensionless numbers)
    characteristic_length: f64,
    /// Characteristic temperature (for conversion to dimensionless quantities)
    characteristic_temperature: f64,
    /// Body force (gravity)
    body_force: [f64; 3],
    /// Fluid parameters
    pub fluid: Fluid,
}
impl SimulationParameters {
    pub fn new(
        // Domain dimension
        dimension: LbmDimension,
        // Requested Mach limit. The higher, the faster the time step (should not be higher than 0.3 for stability)
        mach_limit: f64,
        // Characteristic length of the problem. For Reynolds computation only.
        characteristic_length: f64,
        // Characteristic velocity of the problem. Will affect the time step, and stability.
        characteristic_velocity: f64,
        // Body force (gravity)
        body_force: [f64; 3],
        // Simulated fluid
        fluid: Fluid,
    ) -> Self {
        // Step is voxel size
        let delta_x = dimension.real_size_3d()[0] as f64 / dimension.resolution_3d()[0] as f64;
        // Time step is set to produce a stable simulation, based on requested Mach limit
        let delta_t = mach_limit * delta_x / characteristic_velocity
            * (fluid.gamma / dimension.k_parameter() as f64).sqrt();
        // Fluid at rho_0 will have a dimensionless density of 1.
        let characteristic_mass = fluid.rho_0 * delta_x.powi(3);

        SimulationParameters {
            dimension,
            delta_x,
            delta_t,
            characteristic_mass,
            characteristic_length,
            characteristic_temperature: fluid.temp_0,
            body_force,
            fluid,
        }
    }
    /// Compute the units of a quantity based off of its dimensions' exponents in
    /// the chosen system of characteristic quantities.
    ///
    /// Indices :
    /// * 0 : exponent for mass
    /// * 1 : exponent for length
    /// * 2 : exponent for time
    /// * 3 : exponent for temperature
    pub fn get_dimension_factor(&self, exponents: [i32; 4]) -> f64 {
        self.characteristic_mass.powi(exponents[0])
            * self.delta_x.powi(exponents[1])
            * self.delta_t.powi(exponents[2])
            * self.characteristic_temperature.powi(exponents[3])
    }
    pub fn reynolds_number(&self, characteristic_velocity: f64) -> f64 {
        self.fluid.rho_0 * characteristic_velocity * self.characteristic_length / self.fluid.mu
    }
    pub fn prandtl_number(&self) -> f64 {
        self.fluid.mu * self.fluid.cp / self.fluid.kappa
    }
    pub fn rayleigh_number(&self, delta_temperature: f64) -> f64 {
        let force = self
            .body_force
            .iter()
            .map(|x| x.powi(2))
            .sum::<f64>()
            .sqrt();
        self.fluid.rho_0
            * self.fluid.beta
            * delta_temperature
            * force
            * self.characteristic_length.powi(3)
            / self.fluid.mu
            / self.fluid.kappa
    }
    pub fn kolmogorov_scale(&self, characteristic_velocity: f64) -> f64 {
        self.characteristic_length / self.reynolds_number(characteristic_velocity).powf(0.75)
    }
    pub fn print_dimensionless_number(&self, characteristic_velocity: f64, delta_temperature: f64) {
        let re = self.reynolds_number(characteristic_velocity);
        let ra = self.rayleigh_number(delta_temperature);
        let pr = self.prandtl_number();
        let kolm = self.kolmogorov_scale(characteristic_velocity);
        println!("\nDimensionless numbers:");
        println!(" * Reynolds: {re:1.2e}");
        println!(" * Rayleigh: {ra:1.2e}");
        println!(" * Prandtl: {pr:1.2e}");
        println!("\nKolmogorov scale : {kolm:1.2e} m");
        println!(
            "Optimal cell size for high fidelity reproduction of turbulence : {:1.2e} m",
            kolm / 3.
        );
        println!("Current cell size : {:1.2e} m", self.delta_x);
    }
}

/// Set of parameters used by the Lattice Boltzmann algorithm
#[repr(C)]
#[derive(Default, Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct RawSimulationParameters {
    // Resolution of the mesh
    pub mesh_size: [u32; 3],
    // Time step for each simulation step
    pub time_step: f32,
    // Relaxation time for fluid velocity
    pub relax_f: f32,
    // Relaxation time for thermal dissipation
    pub relax_g: f32,
    // Isobaric thermal expansion coefficient
    pub beta: f32,
    // Body force (gravity). This is a constant field
    pub body_force: [f32; 3],
    // k parameter (mesh dependent)
    pub k: f32,
    // Current simulation time (s)
    pub sim_time: f32,
}
impl RawSimulationParameters {
    pub fn from(params: &SimulationParameters) -> Self {
        // [Length^2 / Time]
        let diffusion_dimension = params.get_dimension_factor([0, 2, -1, 0]);

        // Compute relaxation coefficient for mass transport
        let nu_dimensionless = params.fluid.nu / diffusion_dimension;
        let relax_f = (nu_dimensionless * params.dimension.k_parameter() as f64 + 1. / 2.) as f32;

        // Compute relaxation coefficient for energy transport
        let kappa_dimensionless = params.fluid.kappa / diffusion_dimension;
        let relax_g =
            (kappa_dimensionless * params.dimension.k_parameter() as f64 + 1. / 2.) as f32;

        let acceleration_dim = params.get_dimension_factor([0, 1, -2, 0]);
        let body_force: [f32; 3] = [
            (params.body_force[0] / acceleration_dim) as f32,
            (params.body_force[1] / acceleration_dim) as f32,
            (params.body_force[2] / acceleration_dim) as f32,
        ];
        let beta_dimensionless =
            (params.fluid.beta * params.get_dimension_factor([0, 0, 0, 1])) as f32;

        RawSimulationParameters {
            mesh_size: params.dimension.resolution_u32(),
            time_step: params.delta_t as f32,
            relax_f,
            relax_g,
            beta: beta_dimensionless,
            body_force,
            k: params.dimension.k_parameter(),
            sim_time: 0.,
        }
    }
}
