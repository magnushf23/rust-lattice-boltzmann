use crate::{parameters::SimulationParameters, scene_object::SceneObject};
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::{error::Error, io::BufWriter};

#[derive(Serialize, Deserialize, Debug)]
pub struct ProblemDefinition {
    pub sim_parameters: SimulationParameters,
    pub initial_rho: f32,
    pub initial_temperature: f32,
    pub initial_velocity: [f32; 3],
    pub scene_objects: Vec<SceneObject>,
}

impl ProblemDefinition {
    pub fn from_json<P: AsRef<Path>>(path: P) -> Result<Self, Box<dyn Error>> {
        // Open the file in read-only mode with buffer.
        let file = File::open(path)?;
        let reader = BufReader::new(file);

        // Read the JSON contents of the file as an instance of `ProblemDefinition`.
        let problem_definition = serde_json::from_reader(reader)?;
        Ok(problem_definition)
    }

    pub fn write_json<P: AsRef<Path>>(&self, path: P) -> Result<(), Box<dyn Error>> {
        // Open the file in write mode with buffer.
        let file = File::create(path)?;
        let writer = BufWriter::new(file);
        serde_json::to_writer_pretty(writer, self)?;
        Ok(())
    }
}
