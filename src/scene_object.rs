use serde::{Deserialize, Serialize};

use crate::simulationbuilder::{FlowCondition, NodeType, ThermalCondition};

#[derive(Serialize, Deserialize, Debug)]
pub enum SceneObject {
    Cube {
        label: String,
        ranges: [[usize; 2]; 3],
        node_type: NodeType,
        thermal_condition: ThermalCondition,
        flow_condition: FlowCondition,
    },
    Airfoil {
        label: String,
        center_position: [f64; 3],
        cord: f64,
        naca_code: [f64; 3],
        thermal_condition: ThermalCondition,
    },
    Ball {
        label: String,
        center: [f64; 3],
        radius: f64,
        thermal_condition: ThermalCondition,
        flow_condition: FlowCondition,
    },
}
