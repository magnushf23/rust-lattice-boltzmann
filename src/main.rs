use rust_lattice_boltzmann::core::run;

fn main() {
    pollster::block_on(run());
}

#[cfg(test)]
mod tests {
    use rust_lattice_boltzmann::{dimension, geometry};

    #[test]
    fn draw_line() {
        let mut square = vec![vec![0; 4]; 4];
        let line = geometry::line_2d(2, 3, 1, 0);
        for [x, y] in line.iter() {
            square[*x as usize][*y as usize] = 1;
        }
        let expected = [[0, 0, 0, 0], [1, 1, 1, 0], [0, 0, 0, 1], [0, 0, 0, 0]];

        let symbols = ["□", "■"];
        for y in (0..4).rev() {
            for x in 0..4 {
                print!("{} ", symbols[square[x][y]]);
                // Test if equal to expected
                assert_eq!(square[x][y], expected[x][y]);
            }
            println!();
        }
    }

    #[test]
    fn draw_naca() {
        let mut grid = vec![vec![0; 25]; 50];
        let airfoil = geometry::naca_4_2d(
            dimension::LbmDimension::D2Q9 {
                resolution: [50, 25],
                real_size: [1., 0.5],
            },
            [0.5, 0.25, -0.4],
            0.9,
            [6., 6., 15.],
        );
        for [x, y] in airfoil.iter() {
            grid[*x][*y] = 1;
        }

        let symbols = ["□", "■"];
        for y in (0..25).rev() {
            for x in 0..50 {
                print!("{} ", symbols[grid[x][y]]);
            }
            println!();
        }
    }
}
