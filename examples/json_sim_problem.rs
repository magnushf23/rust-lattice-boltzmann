use std::path::Path;

use rust_lattice_boltzmann::dimension::LbmDimension;
use rust_lattice_boltzmann::parameters::{Fluid, SimulationParameters};
use rust_lattice_boltzmann::problem_definition::ProblemDefinition;
use rust_lattice_boltzmann::scene_object::SceneObject;
use rust_lattice_boltzmann::simulationbuilder::{FlowCondition, NodeType, ThermalCondition};

pub fn main() {
    // Create simulation problem
    let width = 600;
    let height = 300;
    let scale = 0.2;
    let dimension = LbmDimension::D2Q9 {
        resolution: [width, height],
        real_size: [scale * width as f32 / height as f32, scale],
    };

    let fluid = Fluid::air();
    //fluid.kappa = fluid.kappa / 10.;
    let cube_size = 0.08;
    let stream_velocity = 100.;
    let delta_temperature = 50.;

    let sim_parameters = SimulationParameters::new(
        dimension,
        0.3,
        cube_size,
        stream_velocity * 10.,
        [0., 0., 0.],
        fluid,
    );
    sim_parameters.print_dimensionless_number(stream_velocity, delta_temperature);

    let left_wall = SceneObject::Cube {
        label: "Inlet".to_string(),
        ranges: [[0, 1], [1, height - 1], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::TEMPERATURE { temperature: 300. },
        flow_condition: FlowCondition::VELOCITY {
            velocity: [stream_velocity as f32, 0., 0.],
        },
    };
    let right_wall = SceneObject::Cube {
        label: "Outlet".to_string(),
        ranges: [[width - 2, width - 1], [1, height - 1], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::TEMPERATURE { temperature: 300. },
        flow_condition: FlowCondition::PRESSURE { pressure: 100000. },
    };

    let airfoil = SceneObject::Airfoil {
        label: "NACA 6412".to_string(),
        center_position: [0.15, 0.1, -0.05],
        cord: 0.08,
        naca_code: [6., 4., 12.],
        thermal_condition: ThermalCondition::NONE,
    };

    let problem = ProblemDefinition {
        sim_parameters,
        initial_rho: fluid.rho_0 as f32,
        initial_temperature: fluid.temp_0 as f32,
        initial_velocity: [stream_velocity as f32, 0., 0.],
        scene_objects: vec![left_wall, right_wall, airfoil],
    };

    let output_path = Path::new("./examples/fluid_problems/airfoil.json");

    match problem.write_json(output_path) {
        Ok(_) => println!("File saved."),
        Err(err) => println!("{err}"),
    }
}
