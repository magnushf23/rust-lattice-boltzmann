use std::path::Path;

use rust_lattice_boltzmann::dimension::LbmDimension;
use rust_lattice_boltzmann::parameters::{Fluid, SimulationParameters};
use rust_lattice_boltzmann::problem_definition::ProblemDefinition;
use rust_lattice_boltzmann::scene_object::SceneObject;
use rust_lattice_boltzmann::simulationbuilder::{FlowCondition, NodeType, ThermalCondition};

pub fn main() {
    // Create simulation problem
    let width = 1200;
    let height = 600;
    let scale = 0.2;
    let dimension = LbmDimension::D2Q9 {
        resolution: [width, height],
        real_size: [scale * width as f32 / height as f32, scale],
    };

    let fluid = Fluid::air();
    //fluid.kappa = fluid.kappa / 10.;
    let injection_size = scale as f64 * 0.2;
    let injection_velocity = 10.;
    let delta_temperature = 50.;

    let sim_parameters = SimulationParameters::new(
        dimension,
        0.3,
        injection_size,
        injection_velocity * 2.,
        [0., 0., 0.],
        fluid,
    );
    sim_parameters.print_dimensionless_number(injection_velocity, delta_temperature);

    let left_wall = SceneObject::Cube {
        label: "Left wall".to_string(),
        ranges: [[0, 1], [1, height - 1], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::NONE,
        flow_condition: FlowCondition::NONE,
    };
    let right_wall = SceneObject::Cube {
        label: "Right wall".to_string(),
        ranges: [[width - 1, width], [0, height], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::NONE,
        flow_condition: FlowCondition::PRESSURE { pressure: 100000. },
    };
    let bottom_wall = SceneObject::Cube {
        label: "Bottom wall".to_string(),
        ranges: [[1, width], [0, 1], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::NONE,
        flow_condition: FlowCondition::PRESSURE { pressure: 100000. },
    };
    let top_wall = SceneObject::Cube {
        label: "Top wall".to_string(),
        ranges: [[1, width], [height - 1, height], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::NONE,
        flow_condition: FlowCondition::PRESSURE { pressure: 100000. },
    };
    let top = (height as f64 * 0.4).floor() as usize;
    let bottom = (height as f64 * 0.6).ceil() as usize;
    let inlet = SceneObject::Cube {
        label: "Injection inlet".to_string(),
        ranges: [[0, 1], [top, bottom], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::TEMPERATURE { temperature: 350. },
        flow_condition: FlowCondition::VELOCITY {
            velocity: [injection_velocity as f32, 0., 0.],
        },
    };
    let injection_top = SceneObject::Cube {
        label: "Injection top".to_string(),
        ranges: [[0, 30], [top - 6, top], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::NONE,
        flow_condition: FlowCondition::NONE,
    };
    let injection_bottom = SceneObject::Cube {
        label: "Injection bottom".to_string(),
        ranges: [[0, 30], [bottom, bottom + 6], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::NONE,
        flow_condition: FlowCondition::NONE,
    };

    let problem = ProblemDefinition {
        sim_parameters,
        initial_rho: fluid.rho_0 as f32,
        initial_temperature: fluid.temp_0 as f32,
        initial_velocity: [0., 0., 0.],
        scene_objects: vec![
            left_wall,
            right_wall,
            top_wall,
            bottom_wall,
            injection_bottom,
            injection_top,
            inlet,
        ],
    };

    let output_path = Path::new("./examples/fluid_problems/injection.json");

    match problem.write_json(output_path) {
        Ok(_) => println!("File saved."),
        Err(err) => println!("{err}"),
    }
}
