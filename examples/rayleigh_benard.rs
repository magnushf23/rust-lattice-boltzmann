use std::path::Path;

use rust_lattice_boltzmann::dimension::LbmDimension;
use rust_lattice_boltzmann::parameters::{Fluid, SimulationParameters};
use rust_lattice_boltzmann::problem_definition::ProblemDefinition;
use rust_lattice_boltzmann::scene_object::SceneObject;
use rust_lattice_boltzmann::simulationbuilder::{FlowCondition, NodeType, ThermalCondition};

pub fn main() {
    // Create simulation problem
    let width = 1200;
    let height = 600;
    let scale = 0.2;
    let dimension = LbmDimension::D2Q9 {
        resolution: [width, height],
        real_size: [scale * width as f32 / height as f32, scale],
    };

    let fluid = Fluid::air();
    let delta_temperature = 100.;

    let sim_parameters =
        SimulationParameters::new(dimension, 0.3, scale as f64, 0.5, [0., -9.81, 0.], fluid);
    sim_parameters.print_dimensionless_number(0.2, delta_temperature);

    // Left and right walls are implicitly defined as adiabatic non-slip
    let bottom_wall = SceneObject::Cube {
        label: "Bottom wall".to_string(),
        ranges: [[1, width], [0, 1], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::TEMPERATURE {
            temperature: 300. + delta_temperature as f32,
        },
        flow_condition: FlowCondition::NONE,
    };
    let top_wall = SceneObject::Cube {
        label: "Top wall".to_string(),
        ranges: [[1, width], [height - 1, height], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::TEMPERATURE { temperature: 300. },
        flow_condition: FlowCondition::NONE,
    };
    let cube = SceneObject::Cube {
        label: "Top wall".to_string(),
        ranges: [[400, 500], [150, 250], [0, 1]],
        node_type: NodeType::SOLID,
        thermal_condition: ThermalCondition::TEMPERATURE { temperature: 400. },
        flow_condition: FlowCondition::NONE,
    };

    let problem = ProblemDefinition {
        sim_parameters,
        initial_rho: fluid.rho_0 as f32,
        initial_temperature: fluid.temp_0 as f32,
        initial_velocity: [0., 0., 0.],
        scene_objects: vec![top_wall, bottom_wall, cube],
    };

    let output_path = Path::new("./examples/fluid_problems/rayleigh_benard.json");

    match problem.write_json(output_path) {
        Ok(_) => println!("File saved."),
        Err(err) => println!("{err}"),
    }
}
